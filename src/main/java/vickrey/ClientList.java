package vickrey;

import astra.core.ActionParam;
import astra.core.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import vickrey.model.Client;
import vickrey.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ClientList extends Module {
    private static ObjectMapper objectMapper = new ObjectMapper();
    private List<Client> clients = new ArrayList<Client>();

    @TERM
    public Client clientFromJson(String json) {
        try {
            return objectMapper.readValue(json, Client.class);
        } catch (Exception e) {
            System.out.println("Error mapping object");
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public String clientToJson(Client client) {
        try {
            return objectMapper.writeValueAsString(client);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ACTION
    public boolean storeClient(Client client, ActionParam<String> id) {
        id.set("bidder"+clients.size());
        client.setId(""+clients.size());
        clients.add(client);
        return true;
    }

    @ACTION
    public boolean retrieveClient(String id, ActionParam<Client> client) {
        client.set(clients.get(Integer.parseInt(id)));
        return true;
    }

    @TERM
    public String allClientsToJson() {
        try {
            return objectMapper.writeValueAsString(clients);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
