package vickrey;

import astra.core.ActionParam;
import astra.core.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import vickrey.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemList extends Module {
    private static ObjectMapper objectMapper = new ObjectMapper();
    private List<Item> items = new ArrayList<Item>();

    @TERM
    public Item itemFromJson(String json) {
        try {
            return objectMapper.readValue(json, Item.class);
        } catch (Exception e) {
            System.out.println("Error mapping object");
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public String itemToJson(Item item) {
        try {
            return objectMapper.writeValueAsString(item);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public String allItemsToJson() {
        try {
            return objectMapper.writeValueAsString(items);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Module.ACTION
    public boolean storeItem(Item item, ActionParam<String> id) {
        id.set(""+items.size());
        item.setId(""+items.size());
        item.setState("NEW");
        items.add(item);
        return true;
    }

    @Module.ACTION
    public boolean retrieveItem(String id, ActionParam<Item> item) {
        item.set(items.get(Integer.parseInt(id)));
        return true;
    }

    @ACTION
    public boolean printItem(Item item) {
        System.out.println("item: {" + item.getId() + "," + item.getItem() + ")");
        return true;
    }

    @TERM
    public String getItemName(Item item) {
        return item.getItem();
    }

    @TERM
    public int getItemQuantity(Item item) {
        return item.getQuantity();
    }

}
