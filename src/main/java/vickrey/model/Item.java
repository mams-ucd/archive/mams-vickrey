package vickrey.model;

public class Item {
    private String id;
	private String item;
	private int quantity;
	private int reserve;
	private String state;

    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getReserve() {
		return reserve;
	}

	public void setReserve(int reserve) {
		this.reserve = reserve;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String toString() {
    	return "Item: " + item;
	}
}
