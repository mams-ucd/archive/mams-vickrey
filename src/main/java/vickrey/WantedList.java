package vickrey;

import astra.core.ActionParam;
import astra.core.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import vickrey.model.Want;

import java.util.ArrayList;
import java.util.List;

public class WantedList extends Module {
    private static ObjectMapper objectMapper = new ObjectMapper();
    private List<Want> wants = new ArrayList<Want>();

    @TERM
    public Want fromJson(String json) {
        try {
            return objectMapper.readValue(json, Want.class);
        } catch (Exception e) {
            System.out.println("Error mapping object");
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public String toJson(Want want) {
        try {
            return objectMapper.writeValueAsString(want);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @TERM
    public String allToJson() {
        try {
            return objectMapper.writeValueAsString(wants);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Module.ACTION
    public boolean store(Want want, ActionParam<String> id) {
        id.set(""+wants.size());
        want.setId(""+wants.size());
        wants.add(want);
        return true;
    }

    @Module.ACTION
    public boolean retrieve(String id, ActionParam<Want> want) {
        want.set(wants.get(Integer.parseInt(id)));
        return true;
    }

    @ACTION
    public boolean print(Want want) {
        System.out.println("want: {" + want.getId() + "," + want.getItem() + "," + want.getQuantity() + ")");
        return true;
    }

    @TERM
    public String getItem(Want want) {
        return want.getItem();
    }

    @TERM
    public int getQuantity(Want want) {
        return want.getQuantity();
    }

    @TERM
    public int getMaxPrice(Want want) {
        return want.getMaxPrice();
    }
}
