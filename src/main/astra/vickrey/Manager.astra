package vickrey;

import astra.http.*;
import vickrey.model.*;
import vickrey.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

agent Manager extends Auction {
	module Console C;
	module System S;
	module ClientList cl;
    module ItemList il;

    types internal {
        formula interest(string, string);
    }
        
	rule +!main(list args) {
		http.register();
	}

	// ------------------------------------------------------------------------------------------
    // /clients
    // ------------------------------------------------------------------------------------------
    rule $http.post(ChannelHandlerContext ctx, FullHttpRequest req, ["clients"], string mybody) {
        Client client = cl.clientFromJson(mybody);
        cl.storeClient(client, string id);

        !bidder(id);
        wait(created(id, string url));
        C.println("Bidder Strategy exposed at url: " + url);

        ResponseObject obj = http.createResponse();
        http.setStatus(obj, 200);
        http.setLocation(obj, url);
        http.sendResponse(ctx, req, obj);
    }

	rule $http.get(ChannelHandlerContext ctx, FullHttpRequest req, ["clients"]) {
        ResponseObject obj = http.createResponse();
        http.setStatus(obj, 200);
        http.setType(obj,"application/json");
        http.setContent(obj, cl.allClientsToJson());
        http.sendResponse(ctx, req, obj);
    }

    rule @message(subscribe, string sender, interest(string item)) {
        +interest(sender, item);
        send(agree, sender, interest(item));
    }

    rule +!bidder(string id) {
        S.createAgent(id, "vickrey.Bidder");
        S.setMainGoal(id, [S.name()]);
    }

    rule @message(inform, string sender, created(string id, string url)) {
        C.println("received confirmation from: " + sender);
        +created(id, url);
    }
    
    // ------------------------------------------------------------------------------------------
    // /items
    // ------------------------------------------------------------------------------------------
    rule $http.post(ChannelHandlerContext ctx, FullHttpRequest req, ["items"], string mybody) {
        Item item = il.itemFromJson(mybody);
	    il.storeItem(item, string id);

        !!auctionItem(id, il.getItemName(item), il.getItemQuantity(item));

        ResponseObject obj = http.createResponse();
        http.setStatus(obj, 200);
        http.setLocation(obj, http.myAddress()+"/items/"+id);
        http.sendResponse(ctx, req, obj);
    }

    synchronized rule +!auctionItem(string id, string item, int quantity) {
        string auctioneerName = "auctioneer"+id;
        !auctioneer(auctioneerName, item, quantity);
        foreach (interest(string name, item)) {
            send(inform, name, available(item, auctioneerName));
        }
    }

    rule +!auctioneer(string auctioneerName, string item, int quantity) {
        S.createAgent(auctioneerName, "vickrey.Auctioneer");
        S.setMainGoal(auctioneerName, [item, quantity]);
    }

	rule $http.get(ChannelHandlerContext ctx, FullHttpRequest req, ["items"]) {
        ResponseObject obj = http.createResponse();
        http.setStatus(obj, 200);
        http.setType(obj,"application/json");
        http.setContent(obj, il.allItemsToJson());
        http.sendResponse(ctx, req, obj);
    }

	rule $http.get(ChannelHandlerContext ctx, FullHttpRequest req, ["items", string id]) {
        il.retrieveItem(id, Item item);

        ResponseObject obj = http.createResponse();
        http.setStatus(obj, 200);
        http.setType(obj,"application/json");
        http.setContent(obj, il.itemToJson(item));
        http.sendResponse(ctx, req, obj);
    }
}